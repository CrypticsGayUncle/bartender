# drinks.py
drink_list = [
    {
        "name": "Skinny Bitch",
        "ingredients": {
            "rum": 50,
            "coke": 150
        }
    }, {
        "name": "Wein",
        "ingredients": {
            "gin": 50,
            "tonic": 150
        }
    }, {
        "name": "Weinschorle",
        "ingredients": {
            "gin": 15,
            "rum": 15,
            "vodka": 15,
            "tequila": 15,
            "coke": 100,
            "oj": 30
        }
    }, {
        "name": "Vodka-E",
        "ingredients": {
            "vodka": 50,
            "oj": 150
        }
    }, 
     
    
]

drink_options = [
    {"name": "Gin", "value": "gin"},
    {"name": "Rum", "value": "rum"},
    {"name": "Vodka", "value": "vodka"},
    {"name": "Tequila", "value": "tequila"},
    {"name": "Tonic Water", "value": "tonic"},
    {"name": "Coke", "value": "coke"},
    {"name": "Orange Juice", "value": "oj"},
    {"name": "Margarita Mix", "value": "mmix"}
]