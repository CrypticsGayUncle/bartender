from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1306, sh1106
import time
import RPi.GPIO as GPIO
from PIL import ImageFont, ImageDraw
from drinks import drink_list
from relay import makedrink
#buttonconfig
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(13,GPIO.IN,pull_up_down=GPIO.PUD_UP)
prev_input=1 #buttton press erkennen
#13 bcm port for button anschluss

#2. button

GPIO.setup(5,GPIO.IN,pull_up_down=GPIO.PUD_UP)
butt2=1

#displayconfig
serial = i2c(port=1, address=0x3C)
device = sh1106(serial, rotate=0)
screen_height=63
screen_width=125
#font
fnt=ImageFont.truetype("/home/pi/Downloads/bartender/Cleanwork.ttf",18)
#store current drink


i=0;
starting=False

running=False
def menu(text):
    with canvas(device) as draw:
        align = draw.textsize(text,font=fnt)
        y1= 31-(align[1]/2)
        x1= 63-(align[0]/2)
        draw.rectangle((0,0,125,63),outline="white",fill="black")
        #draw.rectangle((1,1,124,62),outline="white",fill="black")
        draw.text((x1,y1),text,fill="white",font=fnt)
    while running:
        time.sleep(0.1)

while True:
    if(not starting):
        menu("Starting..")
        time.sleep(2)
        menu("Ready")
        time.sleep(.5)
        menu(drink_list[i]["name"])
        starting=True
        i+=1
    input = GPIO.input(13)
    if((not prev_input)and input):
        drink=drink_list[i]["name"]
        if(not running):
            menu(drink)
            
            running=True
        else:
            
            running= False
            menu(drink)
            running=True
            
        i+=1
    if(i==len(drink_list)):
        i=0
    prev_input=input
    time.sleep(0.1)
    
    input = GPIO.input(5)
    if((not butt2) and input):
        print(drink_list[i-1]["name"])
        makedrink(drink_list[i-1]["name"])
    butt2= input
        